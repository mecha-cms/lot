---
title: LESS
description: Convert LESS syntax into CSS syntax.
...

This extension compiles LESS code into CSS code by looking for files with extension `less` added through the `Asset::set()` method, storing the compiled results as a static file and then displaying it as a CSS file. The compiled file content will be updated automatically on every file modification changes on the LESS file.

~~~ .txt
.\path\to\file.less → .\path\to\file.css
.\path\to\less\file.less → .\path\to\css\file.css
~~~

### Usage

~~~ .php
Asset::set('.\path\to\file.less');
~~~

[connect:plugin/less]