---
title: SCSS
description: Convert SCSS syntax into CSS syntax.
...

This extension compiles SCSS code into CSS code by looking for files with extension `scss` added through the `Asset::set()` method, storing the compiled results as a static file and then displaying it as a CSS file. The compiled file content will be updated automatically on every file modification changes on the SCSS file.

~~~ .txt
.\path\to\file.scss → .\path\to\file.css
.\path\to\scss\file.scss → .\path\to\css\file.css
~~~

### Usage

~~~ .php
Asset::set('.\path\to\file.scss');
~~~

> **Note:** The **scssphp** implements SCSS (3.2.12). It does not implement the SASS syntax, only the SCSS syntax.

[connect:plugin/scss]